function german-quiz
  while true
    set -l line (shuf -n 1 $argv)
    echo $line | vim - -esbnN -c  'norm f:d$' -c 'x!/dev/stderr' 2>&1 > /dev/null
    read -n 1 -P '' > /dev/null 2>&1
    echo $line | vim - -esbnN -c  'norm df:' -c 'x!/dev/stderr' 2>&1 > /dev/null
    echo '===================================================================================================='
  end
end
